import ax from '../common/ax2'

export function getLog (content, crt, total) {
  return ax({
    method: 'post',
    data: {action: 1101, data: {intValues: [crt, total], strValues: [content]}}
  })
}
