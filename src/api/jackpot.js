import ax from '../common/ax2'
import action from './actions'

export function getJackPots (crt, total, gameId, tableName) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_JACKPOT_GETS, data: {strValues: [tableName], intValues: [crt, total, gameId]}}
  })
}

export function getJackPotUsers (crt, total, gameId, userId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_JACKPOT_USER_GETS, data: {strValues: [userId], intValues: [crt, total, gameId]}}
  })
}

export function getJackPotUserJackPots (crt, total, gameId, userId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_JACKPOT_USER_JACKPOT_GETS, data: {strValues: [userId], intValues: [crt, total, gameId]}}
  })
}
