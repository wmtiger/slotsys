import ax from '../common/ax2'
import action from './actions'

export function getDayAnalys (crt, total, sDay, eDay, bussiness, uid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_RECHARGE_DAY_GETS, data: {intValues: [crt, total], longValues: [sDay, eDay, uid], strValues: [bussiness]}}
  })
}

export function getMonthAnalys (crt, total, sMonth, eMonth, bussiness) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_RECHARGE_MONTH_GETS, data: {intValues: [crt, total, sMonth, eMonth], longValues: [bussiness]}}
  })
}

export function getDayTotalAnalys (crt, total, sDay, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_RECHARGE_DAY_TOTAL_GETS, data: {intValues: [crt, total], longValues: [sDay, eDay]}}
  })
}

export function getMonthTotalAnalys (crt, total, sMonth, eMonth) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_RECHARGE_MONTH_TOTAL_GETS, data: {intValues: [crt, total], longValues: [sMonth, eMonth]}}
  })
}
