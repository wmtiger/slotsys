import ax from '../common/ax2'
import action from './actions'

export function getMyPromoters (crt, total, type) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_PROMOTERS_GETS, data: {intValues: [crt, total, type]}}
  })
}

export function addPromoters (uid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_PROMOTERS_ADD, data: {longValues: [uid]}}
  })
}

export function addPPromoters (uid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_PROMOTERS_P_ADD, data: {longValues: [uid]}}
  })
}

export function bindUId (uId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_PROMOTERS_BIND_UID, data: {longValues: [uId]}}
  })
}

export function getMySellRoomCardRecords (crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_ROOM_CARD_RECORD_SELL_GETS, data: {intValues: [crt, total]}}
  })
}

export function getMyBuyRoomCardRecords (crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_ROOM_CARD_RECORD_BUY_GETS, data: {intValues: [crt, total]}}
  })
}

export function addPromoterRoomCardNum (tUid, tKeyNum) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_PROMOTERS_ADD_ROOM_CARD_NUM, data: {longValues: [tUid], intValues: [tKeyNum]}}
  })
}
