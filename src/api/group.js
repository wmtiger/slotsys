import ax from '@/common/ax2'
import action from './actions'
import {setFetchedData, KEYS} from '@/common/constFetchData.js'
import { getVO } from '@/common/proto-vo.js'

/** 获取用户组列表 */
export function getAllUserGroup () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MGROUP_GETS}
  })
}

/** 根据返回的数据，生成vo的列表 */
export function getAllGroupVOList (data) {
  let MGroupVO = getVO('MGroupVO')
  let pv = data.data.data
  let len = pv.data.length
  let vos = []
  for (let i = 0; i < len; i++) {
    let vo = MGroupVO.decode(pv.data[i])
    vos.push({
      gid: vo.gid.toNumber(), gName: vo.gName, except: vo.except, exceptAction: vo.exceptAction
    })
  }
  // setFetchedData(KEYS.ALL_OPERATES, vos)
  return vos
}

/** 添加用户组 */
export function addUserGroup (data) {
  return ax({
    method: 'post',
    // data: {action: action.ACTION.CMP_MGROUP_ADD, data: {strValues: [gName, menuIds, opActions]}}
    data: {action: action.ACTION.CMP_MGROUP_ADD, data: {data: [data]}}
  })
}

/** 修改用户组 */
export function modifyUserGroup (gid, gName, menuIds, opActions) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MGROUP_MODIFY, data: {longValues: [gid], strValues: [gName, menuIds, opActions]}}
  })
}

/** 删除用户组 */
export function delUserGroup (gid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MGROUP_DEL, data: {longValues: [gid]}}
  })
}

/** 获取所有的权限列表 */
export function getAllOperates (gid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_OPERATE_AUTH_GETS}
  })
}

/** 根据返回的数据，生成vo的列表 */
export function getOperateAuthVOList (data) {
  let OperateAuthVO = getVO('OperateAuthVO')
  let vos = []
  let pv = data.data.data
  let len = pv.data.length
  for (let i = 0; i < len; i++) {
    let vo = OperateAuthVO.decode(pv.data[i])
    vos.push({id: vo.id.toNumber(), name: vo.name, action: vo.action})
  }
  setFetchedData(KEYS.ALL_OPERATES, vos)
  return vos
}

/** 修改组成员 */
export function modifyGroupMembers (gid, newUserIds) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MGROUP_USER_MODIFY, data: {strValues: [newUserIds], longValues: [gid]}}
  })
}
