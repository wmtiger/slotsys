import ax from '../common/ax2'
import action from './actions'

export function getRoleDayRecordAnalys (sDay, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_DAY_RECORDS_ANALYS_GETS, data: {intValues: [sDay, eDay]}}
  })
}

export function refreshDayGamePlayerData (sDay, eDay, level, cDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_REFRESH_PLAYER_DATA, data: {intValues: [sDay, eDay, level, cDay]}}
  })
}

export function getMachineData (sDay, gameId, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_EXPORT_MACHINE_BASE_DATA, data: {intValues: [sDay, gameId, eDay]}}
  })
}

export function getBaseOperationData (sDay, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_EXPORT_BASE_OPERATION_DATA, data: {intValues: [sDay, eDay]}}
  })
}

export function getBaseFreeGameData (sDay, liushui, shoushu, wins, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_EXPORT_BASE_FREE_GAME_DATA, data: {intValues: [sDay, eDay], longValues: [liushui, shoushu, wins]}}
  })
}
export function getBasePayGameData (sDay, type, liushui, shoushu, wins, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_EXPORT_BASE_PAY_GAME_DATA, data: {intValues: [sDay, type, eDay], longValues: [liushui, shoushu, wins]}}
  })
}
export function getBaseGameData (sDay, type, gameId, eDay) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_EXPORT_BASE_GAME_DATA, data: {intValues: [sDay, type, gameId, eDay]}}
  })
}
export function getRoleKeepAnalys (sDay, eDay, bussiness, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_GETS, data: {intValues: [sDay, eDay, crt, total], strValues: [bussiness]}}
  })
}

export function getRoleKeepAnalysBussiness () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_BUSSINESS, data: {}}
  })
}

export function getRoleKeepAnalysTest (sDay, eDay, newPlayerType, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_TEST_GETS, data: {intValues: [sDay, eDay, crt, total, newPlayerType]}}
  })
}

export function getDayLiuShuiRank (sDay, eDay, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_DAY_LIUSHUI_GETS, data: {intValues: [crt, total], longValues: [sDay, eDay]}}
  })
}

export function getRoleKeepAnalysArea (sDay, eDay, area, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_AREA_GETS, data: {intValues: [sDay, eDay, crt, total], strValues: [area]}}
  })
}

export function getRoleKeepAnalysLTV (sDay, eDay, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_LTV_GETS, data: {intValues: [sDay, eDay, crt, total]}}
  })
}

export function getRoleKeepAnalysPay (sDay, eDay, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_PAY_GETS, data: {intValues: [sDay, eDay, crt, total]}}
  })
}

export function getRoleBaseInfos (userId, userName, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_BASE_INFO_GETS, data: {strValues: [userId, userName], intValues: [crt, total]}}
  })
}

export function getRoleGameInfos (uid, gameId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_GAME_INFO_GETS, data: {intValues: [crt, total, gameId], longValues: [uid]}}
  })
}

export function getRoleGameInfosTotal (gameId, sDay, eDay, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_ROLE_GAME_INFO_TOTAL_GETS, data: {intValues: [crt, total, gameId], longValues: [sDay, eDay]}}
  })
}

export function getRoleSlotLogs (tUid, gameId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_SLOT_LOG_GETS, data: {intValues: [crt, total, gameId], strValues: [tUid]}}
  })
}

export function getRoleBaseInfo (userId, userName) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_BASE_INFO_GET, data: {strValues: [userId, userName]}}
  })
}

export function getRechargeLogs (userId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_RECHARGE_LOG_GETS, data: {strValues: [userId], intValues: [crt, total]}}
  })
}

export function getRoleKeepTotalAnalys (sDay, eDay, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_GAME_ROLE_KEEP_TOTAL_GETS, data: {intValues: [sDay, eDay, crt, total]}}
  })
}

/***************************************************
 * BetLevel
 ***************************************************/
export function getBetLevels (id, gameId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BET_LEVEL_GETS, data: {intValues: [crt, total, id], longValues: [gameId]}}
  })
}

export function getAllBetLevels () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BET_LEVEL_GETS_ALL, data: {}}
  })
}

export function modifyBetLevel (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BET_LEVEL_MODIFY, data: {data: [data]}}
  })
}

export function importBetLevel (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BET_LEVEL_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * ChangeStatus
 ***************************************************/
export function getChangeStatuses (gameId, machineType, gameType, betId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_CHANGE_STATUS_GETS, data: {intValues: [crt, total, gameId, machineType, gameType, betId]}}
  })
}

export function getAllChangeStatuses () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_CHANGE_STATUS_GETS_ALL, data: {}}
  })
}

export function modifyChangeStatus (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_CHANGE_STATUS_MODIFY, data: {data: [data]}}
  })
}

export function importChangeStatuses (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_CHANGE_STATUS_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * Game
 ***************************************************/
export function getGames (gameId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_GETS, data: {intValues: [crt, total, gameId]}}
  })
}

export function getAllGames () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_GETS_ALL, data: {}}
  })
}

export function modifyGame (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_MODIFY, data: {data: [data]}}
  })
}

export function importGames (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * GameJackPot
 ***************************************************/
export function getGameJackPots (jackpotId, type, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_JACK_POT_GETS, data: {intValues: [crt, total, jackpotId, type]}}
  })
}

export function getAllGameJackPots () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_JACK_POT_GETS_ALL, data: {}}
  })
}

export function modifyGameJackPot (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_JACK_POT_MODIFY, data: {data: [data]}}
  })
}

export function importGameJackPots (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_GAME_JACK_POT_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * Level
 ***************************************************/

export function getLevels (crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_LEVEL_GETS, data: {intValues: [crt, total]}}
  })
}

export function getAllLevels () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_LEVEL_POT_GETS_ALL, data: {}}
  })
}

export function modifyLevel (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_LEVEL_MODIFY, data: {data: [data]}}
  })
}

export function importLevels (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_LEVEL_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * NewPlayerFree
 ***************************************************/
export function getNewPlayerFrees (gameId, groupId, freeTimes, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_FREE_GETS, data: {intValues: [crt, total, gameId, groupId, freeTimes]}}
  })
}

export function getAllNewPlayerFrees () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_FREE_POT_GETS_ALL, data: {}}
  })
}

export function modifyNewPlayerFree (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_FREE_MODIFY, data: {data: [data]}}
  })
}

export function importNewPlayerFrees (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_FREE_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * NewPlayerResult
 ***************************************************/
export function getNewPlayerResults (gameId, newTimes, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_GETS, data: {intValues: [crt, total, gameId, newTimes]}}
  })
}

export function getAllNewPlayerResults () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_POT_GETS_ALL, data: {}}
  })
}

export function modifyNewPlayerResult (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_MODIFY, data: {data: [data]}}
  })
}

export function importNewPlayerResults (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_IMPORT, data: {strValues: [dataStr]}}
  })
}

export function getNewPlayerResultConfigs (crt, total, gameId, gameIndex) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_CONFIG_GETS, data: {intValues: [crt, total, gameId, gameIndex]}}
  })
}

export function getAllNewPlayerResultConfigs () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_CONFIG_GETS_ALL}
  })
}

export function modifyNewPlayerResultConfig (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_CONFIG_MODIFY, data: {data: [data]}}
  })
}

export function importNewPlayerResultConfigs (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_NEW_PLAYER_RESULT_CONFIG_IMPORT, data: {strValues: [dataStr]}}
  })
}

export function getPlayLines (crt, total, id, gameId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_PLAY_LINE_GETS, data: {intValues: [crt, total, id, gameId]}}
  })
}

export function getAllPlayLines () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_PLAY_LINE_GETS_ALL}
  })
}

export function modifyPlayLine (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_PLAY_LINE_MODIFY, data: {data: [data]}}
  })
}

export function importPlayLines (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_PLAY_LINE_IMPORT, data: {strValues: [dataStr]}}
  })
}

export function getServerInfos (crt, total, processId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SERVER_INFO_GETS, data: {intValues: [crt, total, processId]}}
  })
}

export function getAllServerInfos () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SERVER_INFO_GETS_ALL}
  })
}

export function modifyServerInfo (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SERVER_INFO_MODIFY, data: {data: [data]}}
  })
}

export function importServerInfos (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SERVER_INFO_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * Symbols
 ***************************************************/
export function getSymbolses (crt, total, gameId, element) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_GETS, data: {longValues: [gameId], strValues: [element], intValues: [crt, total]}}
  })
}

export function getAllSymbolses () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_GETS_ALL}
  })
}

export function modifySymbols (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_MODIFY, data: {data: [data]}}
  })
}

export function importSymbolses (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * SymbolsProbability
 ***************************************************/
export function getSymbolsProbabilities (crt, total, gameId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_PROBABILITY_GETS, data: {longValues: [gameId], intValues: [crt, total]}}
  })
}

export function getAllSymbolsProbabilities () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_PROBABILITY_GETS_ALL}
  })
}

export function modifySymbolsProbability (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_PROBABILITY_MODIFY, data: {data: [data]}}
  })
}

export function importSymbolsProbabilities (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_PROBABILITY_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * SymbolsSpecial
 ***************************************************/
export function getSymbolsSpecials (crt, total, gameId, element) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_SPECIAL_GETS, data: {longValues: [gameId], strValues: [element], intValues: [crt, total]}}
  })
}

export function getAllSymbolsSpecials () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_SPECIAL_GETS_ALL}
  })
}

export function modifySymbolsSpecial (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_SPECIAL_MODIFY, data: {data: [data]}}
  })
}

export function importSymbolsSpecials (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_SYMBOLS_SPECIAL_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * Task
 ***************************************************/
export function getTasks (crt, total, taskId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_GETS, data: {longValues: [taskId], intValues: [crt, total]}}
  })
}

export function getAllTasks () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_GETS_ALL}
  })
}

export function modifyTask (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_MODIFY, data: {data: [data]}}
  })
}

export function importTasks (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * TaskBox
 ***************************************************/
export function getTaskBoxs (crt, total, boxId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_GETS, data: {longValues: [boxId], intValues: [crt, total]}}
  })
}

export function getAllTaskBoxs () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_GETS_ALL}
  })
}

export function modifyTaskBox (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_MODIFY, data: {data: [data]}}
  })
}

export function importTaskBoxs (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * TaskBoxReward
 ***************************************************/
export function getTaskBoxRewards (crt, total, rewardId) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_REWARD_GETS, data: {longValues: [rewardId], intValues: [crt, total]}}
  })
}

export function getAllTaskBoxRewards () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_REWARD_GETS_ALL}
  })
}

export function modifyTaskBoxReward (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_REWARD_MODIFY, data: {data: [data]}}
  })
}

export function importTaskBoxRewards (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_BOX_REWARD_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * VipStatus
 ***************************************************/
export function getVipStatuses (crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_VIP_STATUS_GETS, data: {intValues: [crt, total]}}
  })
}

export function getAllVipStatuses () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_VIP_STATUS_GETS_ALL}
  })
}

export function modifyVipStatus (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_VIP_STATUS_MODIFY, data: {data: [data]}}
  })
}

export function importVipStatuses (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TASK_VIP_STATUS_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * advertisement
 ***************************************************/
export function getAdvertisements (id, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ADVERTISEMENT_GETS, data: {longValues: [id], intValues: [crt, total]}}
  })
}

export function getAllAdvertisements () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ADVERTISEMENT_GETS_ALL}
  })
}

export function modifyAdvertisement (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ADVERTISEMENT_MODIFY, data: {data: [data]}}
  })
}

export function importAdvertisements (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ADVERTISEMENT_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * Active Event
 ***************************************************/
export function getActiveEvents (id, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ACTIVE_EVENT_GETS, data: {intValues: [crt, total], longValues: [id]}}
  })
}

export function getAllActiveEvents () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ACTIVE_EVENT_GETS_ALL, data: {}}
  })
}

export function modifyActiveEvent (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ACTIVE_EVENT_MODIFY, data: {data: [data]}}
  })
}

export function importActiveEvents (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_ACTIVE_EVENT_IMPORT, data: {strValues: [dataStr]}}
  })
}

/***************************************************
 * BetLevel
 ***************************************************/
export function getBursts (id, gameId, crt, total) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BURST_GETS, data: {intValues: [crt, total, id], longValues: [gameId]}}
  })
}

export function getAllBursts () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BURST_GETS_ALL, data: {}}
  })
}

export function modifyBurst (data) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BURST_MODIFY, data: {data: [data]}}
  })
}

export function importBurst (dataStr) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_BURST_IMPORT, data: {strValues: [dataStr]}}
  })
}

export function getTableDatas (tableName) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TABLE_GETS_ALL, data: {strValues: [tableName]}}
  })
}

export function importTables (tableName, dataStr) {
  return ax({
    method: 'post',
    // data: {action: action.ACTION.CMP_SLOT_CONFIG_TABLE_IMPORT, data: {strValues: [tableName, dataStr[0], dataStr[1], dataStr[2], dataStr[3], dataStr[4], dataStr[5], dataStr[6], dataStr[7], dataStr[8], dataStr[9], dataStr[10]]}}
    data: {action: action.ACTION.CMP_SLOT_CONFIG_TABLE_IMPORT, data: {strValues: [tableName, dataStr]}}
  })
}

export function sendTablePWD (url, tablePWD) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SEND_TABLE_PWD, data: {strValues: [url, tablePWD]}}
  })
}

export function refreshRoleKeepAnalys () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_REFRESH_ROLE_KEEP_ANALYS}
  })
}

export function refreshRoleKeepAnalysArea () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_REFRESH_ROLE_KEEP_ANALYS_AREA}
  })
}

export function refreshRoleKeepAnalysLTV () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_REFRESH_ROLE_KEEP_ANALYS_LTV}
  })
}

export function refreshRoleKeepAnalysPay () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_REFRESH_ROLE_KEEP_ANALYS_PAY}
  })
}

export function sendActiveReward (uIds, activeId, points, jabe, vipExp, title, contents) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_SEND_ACTIVE_REWARD, data: {strValues: [uIds, title, contents], longValues: [activeId, points, jabe, vipExp]}}
  })
}

