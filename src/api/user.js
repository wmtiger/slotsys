import ax from '@/common/ax2'
import action from './actions'

/** 用户登陆 */
export function login (nickName, password) {
  return ax({
    method: 'post',
    data: { action: action.ACTION.LOGIN, data: { strValues: [nickName, password] } }
  })
}

/** 修改密码(未测试) */
export function modifyPwd (oldPwd, newPwd) {
  return ax({
    method: 'post',
    data: {action: 1022, data: {strValues: [oldPwd, newPwd]}}
  })
}

/** 获取成员信息 */
export function getManager () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_GET}
  })
}

/** 获取所有的用户列表 */
export function getAllUser () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_GETS}
  })
}

/** 编辑用户状态 */
export function modifyStatus (uid, status) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_MODIFY_STATUS, data: {longValues: [uid], intValues: [status]}}
  })
}

/** 编辑用户 */
export function modifyManager (uid, userName, realName, gid, status) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_MODIFY, data: {longValues: [uid, gid], strValues: [userName, realName], intValues: [status]}}
  })
}

/** 添加用户 */
export function addManager (userName, realName, passward, gid, status) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_ADD, data: {longValues: [gid], strValues: [userName, realName, passward], intValues: [status]}}
  })
}

/** 删除用户 */
export function delManager (uid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_DEL, data: {longValues: [uid]}}
  })
}

/** 获取组的成员 */
export function getGroupMembers (gid) {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MGROUP_GET_MEMBERS, data: {longValues: [gid]}}
  })
}

/** 获取没有上线成员 */
export function getNoPMembers () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_NOP_GETS, data: {}}
  })
}

/**  */
export function getPMembers () {
  return ax({
    method: 'post',
    data: {action: action.ACTION.CMP_MANAGER_P_GETS, data: {}}
  })
}

