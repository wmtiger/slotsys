/** 获取到的数据，存在这里，用key来读取 */
let fetched = {}
// global.fetched = {}

export let KEYS = {
  /** 所有的菜单列表 */
  ALL_MENUS: 'allMenus',
  /** 我的菜单列表 */
  MY_MENUS: 'myMenus',
  /** 所有的权限操作列表 */
  ALL_OPERATES: 'allOperates'
}

/** 通过key来获取缓存好的数据 */
export function getFetchedData (key) {
  let d = fetched[key]
  return d
}

/** 将需要缓存的数据存入fetched对象 */
export function setFetchedData (key, value) {
  console.log('will set fetched[' + key + ']', fetched[key])
  if (fetched[key]) {
    console.log('fetched[' + key + '] has exist, will override ' + key, fetched)
  }
  fetched[key] = value
}
