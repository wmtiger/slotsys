import Cookies from 'js-cookie'

const TokenKey = 'SLOT-User-Token'
const UserNameKey = 'SLOT-User-Name'
const UserPwdKey = 'SLOT-User-Pwd'
const UserBindUserIdKey = 'SLOT-User-Bind-UserId'
const UserBindUserNameKey = 'SLOT-User-Bind-UserName'
const UserBindUIdKey = 'SLOT-User-Bind-UId'
const UIdKey = 'SLOT-UID'
const UserGidKey = 'SLOT-User-GID'

export function getToken () {
  return Cookies.get(TokenKey)
}

export function setToken (token) {
  return Cookies.set(TokenKey, token, {expires: 1800})
}

export function removeToken () {
  return Cookies.remove(TokenKey)
}

export function setUserName (userName) {
  return Cookies.set(UserNameKey, userName, {expires: 1800})
}

export function getUserName () {
  return Cookies.get(UserNameKey)
}

export function removeUserName () {
  return Cookies.remove(UserNameKey)
}

export function setUserPwd (pwd) {
  return Cookies.set(UserPwdKey, pwd, {expires: 1800})
}

export function getUserPwd () {
  return Cookies.get(UserPwdKey)
}

export function removeUserPwd () {
  return Cookies.remove(UserPwdKey)
}

export function setUserBindUserId (uid) {
  return Cookies.set(UserBindUserIdKey, uid, {expires: 1800})
}

export function getUserBindUserId () {
  return Cookies.get(UserBindUserIdKey)
}

export function removeUserBindUserId () {
  return Cookies.remove(UserBindUserIdKey)
}

export function setUserBindUserName (userName) {
  return Cookies.set(UserBindUserNameKey, userName, {expires: 1800})
}

export function getUserBindUserName () {
  return Cookies.get(UserBindUserNameKey)
}

export function removeUserBindUserName () {
  return Cookies.remove(UserBindUserNameKey)
}

export function setUserBindUId (uId) {
  return Cookies.set(UserBindUIdKey, uId, {expires: 1800})
}

export function getUserBindUId () {
  return Cookies.get(UserBindUIdKey)
}

export function removeUserBindUId () {
  return Cookies.remove(UserBindUIdKey)
}

export function setUId (uId) {
  return Cookies.set(UIdKey, uId, {expires: 1800})
}

export function getUId () {
  return Cookies.get(UIdKey)
}

export function removeUId () {
  return Cookies.remove(UIdKey)
}

export function setUserGid (gid) {
  return Cookies.set(UserGidKey, gid, {expires: 1800})
}

export function getUserGid () {
  return Cookies.get(UserGidKey)
}

export function removeUserGid () {
  return Cookies.remove(UserGidKey)
}
