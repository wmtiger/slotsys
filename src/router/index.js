import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

let route = new Router({
  mode: 'hash',
  base: process.env.NODE_ENV === 'production' ? '/slotmanager/' : '',
  routes: [
    {
      path: '/',
      // component: resolve => require(['../pages/Login.vue'], resolve)
      redirect: to => {
        // todo: 这里要改成根据是否有user的token来判断重定向路径
        let token = store.getters.token
        if (token && token.length > 0) {
          return {path: '/dashboard'}
        } else {
          return {path: '/login'}
        }
      }
    },
    {
      path: '/login',
      component: resolve => require(['@/pages/login.vue'], resolve)
      // component: resolve => require(['@/pages/login_pc.vue'], resolve)
    },
    {
      path: '/forget',
      component: resolve => require(['@/pages/forget.vue'], resolve)
    },
    {
      path: '/dashboardagent',
      component: resolve => require(['@/pages/dashboardagent'], resolve),
      children: [
        {
          path: '/PromotersAgent',
          component: resolve => require(['@/pages/promotersagent1'], resolve),
          children: [
            {
              path: '/PromotersMain',
              component: resolve => require(['@/pages/promotersagent1/promotersmain'], resolve)
            },
            {
              path: '/PromotersSellRecord',
              component: resolve => require(['@/pages/promotersagent1/promoterssellrecord'], resolve)
            },
            {
              path: '/PromotersBuyRecord',
              component: resolve => require(['@/pages/promotersagent1/promotersbuyrecord'], resolve)
            },
            {
              path: '/AddPromotersJabe',
              component: resolve => require(['@/pages/promotersagent1/addPromotersJabe'], resolve)
            },
            {
              path: '/AddPromotersJabeSuccess',
              component: resolve => require(['@/pages/promotersagent1/addPromotersJabeSuccess'], resolve)
            },
            {
              path: '/AddPromotersJabeFail',
              component: resolve => require(['@/pages/promotersagent1/addPromotersJabeFail'], resolve)
            }
          ]
        }
      ]
    },
    {
      path: '/dashboard',
      component: resolve => require(['@/pages/dashboard'], resolve),
      children: [
        {
          path: '/OperationLog',
          component: resolve => require(['@/pages/mgr/log'], resolve)
        },
        {
          path: '/User',
          component: resolve => require(['@/pages/mgr/user'], resolve)
        },
        {
          path: '/UserGroup',
          component: resolve => require(['@/pages/mgr/usergroup'], resolve)
        },
        {
          path: '/Menu',
          component: resolve => require(['@/pages/mgr/menumgr'], resolve)
        },
        {
          path: '/ModifyPWD',
          component: resolve => require(['@/pages/mgr/modifypwd'], resolve)
        },
        {
          path: '/RoleKeepAnalys',
          component: resolve => require(['@/pages/basedata/rolekeepanalys'], resolve)
        },
        {
          path: '/RoleKeepAnalysTest',
          component: resolve => require(['@/pages/basedata/rolekeepanalystest'], resolve)
        },
        {
          path: '/RoleKeepAnalysArea',
          component: resolve => require(['@/pages/basedata/rolekeepanalysarea'], resolve)
        },
        {
          path: '/RoleKeepAnalysLTV',
          component: resolve => require(['@/pages/basedata/rolekeepanalysltv'], resolve)
        },
        {
          path: '/RoleKeepAnalysPay',
          component: resolve => require(['@/pages/basedata/rolekeepanalyspay'], resolve)
        },
        {
          path: '/RoleBaseInfos',
          component: resolve => require(['@/pages/basedata/rolebaseinfos'], resolve)
        },
        {
          path: '/RoleBaseInfo',
          component: resolve => require(['@/pages/basedata/rolebaseinfo'], resolve)
        },
        {
          path: '/RoleGameInfos',
          component: resolve => require(['@/pages/basedata/rolegameinfos'], resolve)
        },
        {
          path: '/RoleGameInfosTotal',
          component: resolve => require(['@/pages/basedata/rolegameinfostotal'], resolve)
        },
        {
          path: '/RoleSlotLogs',
          component: resolve => require(['@/pages/basedata/roleslotlogs'], resolve)
        },
        {
          path: '/RoleDayGameAnalys',
          component: resolve => require(['@/pages/basedata/roledaygameanalys'], resolve)
        },
        {
          path: '/DayAnalys',
          component: resolve => require(['@/pages/recharge/dayanalys'], resolve)
        },
        {
          path: '/MonthAnalys',
          component: resolve => require(['@/pages/recharge/monthanalys'], resolve)
        },
        {
          path: '/DayTotalAnalys',
          component: resolve => require(['@/pages/recharge/daytotalanalys'], resolve)
        },
        {
          path: '/MonthTotalAnalys',
          component: resolve => require(['@/pages/recharge/monthtotalanalys'], resolve)
        },
        {
          path: '/JackPotData',
          component: resolve => require(['@/pages/jackpot/jackpots'], resolve)
        },
        {
          path: '/JackPotUser',
          component: resolve => require(['@/pages/jackpot/jackpotusers'], resolve)
        },
        {
          path: '/JackPotUserJackPot',
          component: resolve => require(['@/pages/jackpot/jackpotusersjackpots'], resolve)
        },
        {
          path: '/BetLevel',
          component: resolve => require(['@/pages/slotconfig/betlevel'], resolve)
        },
        {
          path: '/ChangeStatus',
          component: resolve => require(['@/pages/slotconfig/changestatus'], resolve)
        },
        {
          path: '/Game',
          component: resolve => require(['@/pages/slotconfig/game'], resolve)
        },
        {
          path: '/GameJackPot',
          component: resolve => require(['@/pages/slotconfig/gamejackpot'], resolve)
        },
        {
          path: '/Level',
          component: resolve => require(['@/pages/slotconfig/level'], resolve)
        },
        {
          path: '/NewPlayerFree',
          component: resolve => require(['@/pages/slotconfig/newplayerfree'], resolve)
        },
        {
          path: '/NewPlayerResult',
          component: resolve => require(['@/pages/slotconfig/newplayerresult'], resolve)
        },
        {
          path: '/NewPlayerResultConfig',
          component: resolve => require(['@/pages/slotconfig/newplayerresultconfig'], resolve)
        },
        {
          path: '/PlayLine',
          component: resolve => require(['@/pages/slotconfig/playline'], resolve)
        },
        {
          path: '/ServerInfo',
          component: resolve => require(['@/pages/slotconfig/serverinfo'], resolve)
        },
        {
          path: '/Symbols',
          component: resolve => require(['@/pages/slotconfig/symbols'], resolve)
        },
        {
          path: '/SymbolsProbability',
          component: resolve => require(['@/pages/slotconfig/symbolsprobability'], resolve)
        },
        {
          path: '/Task',
          component: resolve => require(['@/pages/slotconfig/task'], resolve)
        },
        {
          path: '/TaskBox',
          component: resolve => require(['@/pages/slotconfig/taskbox'], resolve)
        },
        {
          path: '/TaskBoxReward',
          component: resolve => require(['@/pages/slotconfig/taskboxreward'], resolve)
        },
        {
          path: '/VipStatus',
          component: resolve => require(['@/pages/slotconfig/vipstatus'], resolve)
        },
        {
          path: '/Advertisement',
          component: resolve => require(['@/pages/slotconfig/advertisement'], resolve)
        },
        {
          path: '/ActiveEvent',
          component: resolve => require(['@/pages/slotconfig/activeEvent'], resolve)
        },
        {
          path: '/SymbolsSpecial',
          component: resolve => require(['@/pages/slotconfig/symbolsspecial'], resolve)
        },
        {
          path: '/SlotsTable',
          component: resolve => require(['@/pages/slotconfig/slotstable'], resolve)
        },
        {
          path: '/RoleKeepTotalAnalys',
          component: resolve => require(['@/pages/basedata/rolekeeptotalanalys'], resolve)
        },
        {
          path: '/RoleDayRecordAnalys',
          component: resolve => require(['@/pages/basedata/roledayrecordanalys'], resolve)
        },
        {
          path: '/RoleAccountLock',
          component: resolve => require(['@/pages/gmmgr/roleaccountlock'], resolve)
        },
        {
          path: '/RoleRecharge',
          component: resolve => require(['@/pages/gmmgr/rolerecharge'], resolve)
        },
        {
          path: '/Promoters',
          // component: resolve => require(['@/pages/promoters'], resolve)
          redirect: to => {
            return {path: '/dashboardagent'}
          }
        },
        {
          path: '/SendTablePWD',
          component: resolve => require(['@/pages/gmmgr/tablePWD'], resolve)
        },
        {
          path: '/SendActiveReward',
          component: resolve => require(['@/pages/gmmgr/sendactivereward'], resolve)
        }
      ]
    },
    { path: '/404',
      component: resolve => require(['@/pages/404.vue'], resolve)
    },
    { path: '*', redirect: '/login' }
  ]
})

console.log('route.options', route.options, process.env.NODE_ENV)

export default route
