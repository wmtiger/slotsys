# matsys

> A Test project

## 安装环境
``` bash
# 首先安装nodejs，下载msi，安装后，自动集成了npm! 注意环境变量
# 先使用npm安装好vue和vue-cli以及yarn(这个是npm的替代品)
npm i -g vue vue-cli yarn 

```

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080 开启服务器
yarn run dev

# build for production with minification 打包
yarn run build

# build for production and view the bundle analyzer report
# npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 开发记录

第一次提交，主要将工作目录提交上去，直接使用命令：

``` bash
# webpack后面跟着名字就是新建个目录，如果不跟，就是将目录创建在当前目录
vue init webpack
```

#说明
添加页面需要 在router->index.js中添加相应的配置 path是menu表中的menuName字段去空格require括号里配置的是页面路径
开发的配置文件:config/dev.env.js,发布的配置:config/prod.env.js
端口的修改:config/index.env.js,开发版本的端口在dev下面修改，发布版本的build里修改